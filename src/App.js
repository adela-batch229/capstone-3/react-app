import {useState, useEffect} from 'react'
import './App.css';

// App nav bar
import AppNavbar from './components/AppNavBar.js'

// Home
import Home from './pages/Home.js'

//User Page
import Products from './pages/UserPage.js'

//item view
import ItemView from './components/ItemView.js'

//user profile
import Profile from './pages/Profile.js'

//register
import Register from './pages/Register.js'

// login
import Login from './pages/Login.js'

//logout
import Logout from './pages/Logout.js'

//admin Dashboard
import AdminPage from './pages/AdminPage.js'

//Admin add product
import AddProduct from './pages/AddProduct.js'

//Order History
import Orders from './pages/AllOrders.js'

//not found
import NotFound from './pages/NotFound.js'

//update product
import UpdateProduct from './pages/UpdateProduct.js'

import {Container} from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

//User context
import {UserProvider} from './UserContext.js'

function App() {
  const [user, setUser] = useState({

    token:localStorage.getItem('token'),
    isAdmin:localStorage.getItem('isAdmin')

  })

  const unsetUser = () => {
    localStorage.clear()
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route exact path ="/" element={<Home/>}/>
            <Route exact path ="/products" element={<AdminPage/>}/>
            <Route exact path ="/active-items" element={<Products/>}/>
            <Route exact path ="/profile" element={<Profile/>}/>
            <Route exact path ="/search/:itemId" element={<ItemView/>}/>
            <Route exact path ="/login" element={<Login/>}/>
            <Route exact path ="/register" element={<Register/>}/>
            <Route exact path ="/logout" element={<Logout/>}/>
            <Route exact path ="/addProduct" element={<AddProduct/>}/>
            <Route exact path ="/allOrders" element={<Orders/>}/>
            <Route exact path ="/update/:itemId" element={<UpdateProduct/>}/>
            <Route exact path ="/*" element={<NotFound/>}/>
            <Route exact path ="/update/:itemId" element={<UpdateProduct/>}/>
          </Routes>
        </Container>
      </Router>
      </>
    </UserProvider>
    )
}

export default App;
