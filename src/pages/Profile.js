import UserOrders from '../components/UserOrders.js'
import {Card} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

export default function Profile(){

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			method: 'POST',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNo(data.mobileNo)
		})
	}, [])

	const [orders,setOrders] =useState([])

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/orders/getOrders/`, {
			method: 'GET',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setOrders(data.map(order =>{
				return(
					<UserOrders key ={order._id} orderProps={order}/>
					)
			}).reverse())
	
		})
	}, [])

	return(
		<>
			<Card className ='mx-4 my-3 p-3'>
				<Card.Body>
					<Card.Text className= 'mb-0'>First Name: {firstName}</Card.Text>
					<Card.Text className= 'mb-0'>Last Name: {lastName}</Card.Text>
					<Card.Text className= 'mb-0'>Email: {email}</Card.Text>
					<Card.Text className= 'mb-0'>Mobile No: {mobileNo}</Card.Text>
				</Card.Body>
			</Card>
			<h5 className ='mx-4'>My orders</h5>
			{orders}
		</>

		)
}