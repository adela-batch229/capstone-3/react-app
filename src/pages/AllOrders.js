import OrderHistory from '../components/OrderHistory.js'
import {Button} from 'react-bootstrap';
import {Navigate, Link, useNavigate} from 'react-router-dom'
import {useEffect, useState} from 'react'

export default function Orders(){
	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/orders/getAllOrders`, {
			method: 'GET',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setOrders(data.map(orders =>{
				return(
					<OrderHistory key={orders._id} orderProps={orders}/>
					)
			}).reverse())
		})
	}, [])

	return(
		<>
			<h1 className='text-center'>All Orders</h1>
			<Link className= 'btn btn-dark mx-4' to ={`/products`}>Back</Link>
			{orders}
		</>


		)

}