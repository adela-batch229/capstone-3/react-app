import {useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js'

export default function Logout(){

	// consume the usercontext object and desctructure it to access the user state and unsetuser function from the context provider
	// deconstructing unsetUser from app.js
	const {unsetUser, setUser} = useContext(UserContext);

	//Clear the localStorage of the user's information 
	// method from app.js
	unsetUser(); //localStorage.clear()


	useEffect(() => {

		//allows us to set the user state back to its original value which is ''
		setUser({id:null})
	})

	const refreshPage = () =>{
		window.location.reload(true);
	}

	return(
			<>
			<Navigate to = "/"/>
			{refreshPage}
			</>
		)
}