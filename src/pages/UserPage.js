import ActiveItems from '../components/ActiveItems.js'
import {Button} from 'react-bootstrap';
import {Navigate, Link, useNavigate} from 'react-router-dom'
import {useEffect, useState} from 'react'

export default function Products(){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/active-items`, {
			method: 'GET',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					<ActiveItems key={product._id} productProps={product}/>
					)
			}))
		})
	}, [])

	return(
		<>
			<h1 className='text-center'>Welcome To Scentalized!</h1>
			{products}
		</>


		)

}