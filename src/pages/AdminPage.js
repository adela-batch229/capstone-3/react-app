import AdminDashboard from '../components/AdminDashboard.js'
import {Button} from 'react-bootstrap';
import {Navigate, Link, useNavigate} from 'react-router-dom'
import {useEffect, useState} from 'react'

export default function Products(){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/allItems`, {
			method: 'GET',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					<AdminDashboard key={product._id} productProps={product}/>
					)
			}))
		})
	}, [])

	return(
		<>
			<h1 className='text-center'>All Products</h1>
			<Link className= 'btn btn-dark mx-4' to ={`/addProduct`}>Add product</Link>
			<Link className= 'btn btn-dark' to ={`/allOrders`}>Order History</Link>
			{products}
		</>

		)
}