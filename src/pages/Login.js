import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Login(){

	const {user, setUser} = useContext(UserContext);

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive ] = useState(false);

	console.log(email);
	console.log(password)

	useEffect(() => {
		// Validation
		if((email !== '' && password !== '')){
			setIsActive(true)
		} else{
			setIsActive(false)
		}
	}, [email, password]);

	function authenticate(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					icon:"success",
					title: "Login Successful!",
					text: "Welcome Back!."
				})
			} else {
				Swal.fire({
					icon:"error",
					title: "Authentication Failed!",
					text:" Check credentials and try again."
				})
			}
		})

		setEmail('');
		setPassword('');
	}

	const retrieveUserDetails = (token) => {
		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs

		fetch('http://localhost:4000/users/details', {
			method:"POST",
			headers:{
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				_id:user.id
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	return(

		(user.token !== null)?

			<Navigate to="/active-items"/>
			:
			<>
				<h1>Login</h1>
				<Form onSubmit = {(e) =>authenticate(e)}>
			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="name@mail.com"
			        	value={email}   
			        	onChange = {e => setEmail(e.target.value)}  
			        	required
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password" 
			        	value={password} 
			        	onChange = {e => setPassword(e.target.value)} 
			        	required
			        />
			      </Form.Group>

					{/*Conditional rendering -> if active, button is clickable, if inactive, button is not clickable*/}
					{
						(isActive) ? 
						<Button variant="dark" type="submit" controlId="submitBtn">
						Login
						</Button>
						:
						<Button variant="secondary" type="submit" controlId="submitBtn" disabled>
						Login
						</Button>
					}	      
			    </Form>  
		    </>
			)
}