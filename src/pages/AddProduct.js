import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {Navigate, Link, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Create() {
	const navigate = useNavigate

	const [productName, setProductName] = useState();
	const [productDesc, setProductDesc] = useState();
	const [productPrice, setProductPrice] = useState();

	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {
		if(productName !=='' && productDesc !=='' && productPrice !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	})


	const addProduct =(e) => {
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/products/create`, {
			method: "POST",
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: productName,
				description: productDesc,
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title:"Created!",
					icon:"success",
					text:"You have successfully created a product"
				})
				setProductName('')
				setProductDesc('')
				setProductPrice('')
			} else {
				Swal.fire({
					title: "Ugh! Sorry.",
					icon:"error",
					text:"Please try again"
				})
			}
		})
	}

	return(
		<Form onSubmit = {(e) =>addProduct(e)}>
			<h1>Add Product</h1>
			<Form.Group className="mb-1" controlId="pName">
				<Form.Label>Product Name</Form.Label>
				<Form.Control 
				type ='text' 
				placeholder='Product name'
				value={productName}
				onChange = {e => setProductName(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group className="mb-1" controlId="pDesc">
				<Form.Label>Description</Form.Label>
				<Form.Control 
				type ='text' 
				placeholder='Description'
				value={productDesc}
				onChange = {e => setProductDesc(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group className="mb-1" controlId="pPrice">
				<Form.Label>Price</Form.Label>
				<Form.Control 
				type ='number' 
				placeholder=''
				value={productPrice}
				onChange = {e => setProductPrice(e.target.value)} 
				required/>
			</Form.Group>

			{/*Conditional rendering -> if active, button is clickable, if inactive, button is not clickable*/}
			{
				(isActive) ? 
				<Button variant="dark" type="submit" controlId="submitBtn">
			Create
				</Button>
				:
				<Button variant="dark" type="submit" controlId="submitBtn" disabled>
			Create
				</Button>
			}

			<Link className= 'btn btn-secondary m-3'to='/products'> Back </Link>

		</Form>
		)
}