import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function Dashboard({productProps}){

	const{name,description,price,_id,isActive}= productProps;

	const activate = (e) => {
		e.preventDefault()
		console.log("hello")
		fetch(`${ process.env.REACT_APP_API_URL }/products/activate/${productProps._id}`,{
			method:"PUT",
			headers:{
				"Content-Type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

		})
	}

	const archive = (e) => {
		e.preventDefault()
		console.log("hello")
		fetch(`${ process.env.REACT_APP_API_URL }/products/archive/${productProps._id}`,{
			method:"PUT",
			headers:{
				"Content-Type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

		})
	}

	return(
		(productProps.isActive)?
		<Form onSubmit ={(e) => archive(e)} >
		<Card className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Link className= 'btn btn-dark' to ={`/update/${_id}`}>Update</Link>
	 			<Button variant="secondary" type="submit" controllId = "submitBtn" className= "mx-2">
					Deactivate
				</Button>
			</Card.Body>
		</Card>
		</Form>
		:
		<Form onSubmit ={(e) => activate(e)}>
		<Card  className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Link className= 'btn btn-dark' to ={`/update/${_id}`}>Update</Link>
	 			<Button variant="light" type="submit" controllId = "submitBtn" className= "mx-2">
					Re-activate
				</Button>
			</Card.Body>
		</Card>
		</Form>

		)
}