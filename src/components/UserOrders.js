import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function Dashboard({orderProps}){
	const{purchasedOn,_id,products,totalAmount}= orderProps;

	return(
		<Card className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Title>Date: {purchasedOn}</Card.Title>
				<Card.Subtitle className="mb-0 mt-3">Order Id: {_id}</Card.Subtitle>
				<Card.Text className="mb-0">Product: {products.productId} Quantity: {products.quantity}</Card.Text>
				<Card.Text>Total: {totalAmount}</Card.Text>
			</Card.Body>
		</Card>

		)
}