import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner(){
	return(
		<Row>
			<Col className = "text-center p-5">
				<h1>Scentalized</h1>
				<p>Fragrance for your home!</p>
				<Link className= 'btn btn-dark mb-4' to ={`/active-items`}>Shop Now!</Link>
				<img className='container-fluid'width ='100' height ='630' src='https://img.freepik.com/free-photo/cozy-composition-with-flaming-candles-young-tree-branches-wooden-surface-scandinavian-style_169016-11275.jpg?w=1380&t=st=1675745001~exp=1675745601~hmac=d38d40aaf7abae3f0d52362617865005f358c20ea269c87cf241e174e11b76d5'/>
			</Col>
		</Row>
		)
}