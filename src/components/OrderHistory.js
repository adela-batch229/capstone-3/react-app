import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function Dashboard({orderProps}){
	const{purchasedOn,_id,products,totalAmount,userId}= orderProps;

	return(
		<Card className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Subtitle>User: {userId}</Card.Subtitle>
				<Card.Text className="mb-0">Order Id: {_id}</Card.Text>
				<Card.Text className="mb-0">Product: {products.productId} Quantity: {products.quantity}</Card.Text>
				<Card.Text>Total: {totalAmount}, Purchased on: {purchasedOn}</Card.Text>
			</Card.Body>
		</Card>

		)
}