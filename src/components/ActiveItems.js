import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function Dashboard({productProps}){
	const{name,description,price,_id,isActive}= productProps;

	return(
		<Card className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>PHP {price}</Card.Text>
	            <Link className="btn btn-dark" to ={`/search/${_id}`}>Details</Link>
			</Card.Body>
		</Card>

		)
}