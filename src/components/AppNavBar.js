import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Container, Row, Col} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){
	const {user} = useContext(UserContext);
	
	return(
		<Container fluid className='font-roboto'>
			<Row>
				<Navbar bg='light' expand = 'lg' className="p-3 sticky-top">
					<Navbar.Brand as={NavLink} exact to="/">Scentalized</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav"/>
					<Navbar.Collapse id="basic-navbar-nav">
						<Col lg={{ span: 3, offset: 0 }}>
							<Nav className="ml-auto">
								<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
								
								{
									(user.token !==null) ?
										(user.isAdmin === true)?
										<>
										<Nav.Link as={NavLink} exact to="/active-items">Products</Nav.Link>
										<Nav.Link as={NavLink} exact to="/products">Admin</Nav.Link>
										<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
										</>
										:
										<>
										<Nav.Link as={NavLink} exact to="/active-items">Products</Nav.Link>
										<Nav.Link as={NavLink} exact to="/profile">My Profile</Nav.Link>
										<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
										</>
									:
									<>
										<Nav.Link as={NavLink} exact to="/active-items">Products</Nav.Link>
										<Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
										<Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
									</>
								}
							</Nav>
						</Col>
						<Col lg={{ span: 1, offset: 7}}>
						<Nav>
							<Nav.Link as={NavLink} exact to="https://shopee.ph/scentalized" target="_blank">Shoppee</Nav.Link>
							<Nav.Link as={NavLink} exact to="https://www.lazada.com.ph/shop/scentalized" target="_blank">Lazada</Nav.Link>
							<Nav.Link as={NavLink} exact to="https://www.tiktok.com/@scentalized" target="_blank">Tiktok</Nav.Link>
						</Nav>
					</Col>
					</Navbar.Collapse>
					
				</Navbar>
			</Row>
		</Container>
		)
}


