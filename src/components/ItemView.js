import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'


export default function ItemView(){

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const { user } = useContext(UserContext);

	//the "useParams" hook allows us to retrieve the courseId passed via URL

	const { itemId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0)

	const [quantity, setQuantity] = useState('')
	const [total, setTotal] = useState('')

	useEffect(() => {
		console.log(itemId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/search/${itemId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);

			});
	}, [])

	const addToCart = (e) =>{
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/orders/createOrder`,{
			method:"POST",
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				product:{
					productId: itemId,
					quantity: quantity
				}
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true){
			Swal.fire({
					title:"Thank You!",
					icon:"success",
					text: "You have successfully created an order"
				})
				navigate('/active-items')
			} else {
				Swal.fire({
					title: "Agh!",
					icon:"error",
					text:"Please Try again"
				})
			}
		})
		
	}

	return(
		<Form onSubmit = {(e) => addToCart(e)}>
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{
								(user.token !== null)?
									(user.isAdmin == false)?
									<>
									<Container fluid="sm">
										<Row>
											<Col>
												<Form.Group className ='mb-1' controllId='quantity'>
													<Form.Label>Quantity: </Form.Label>
													<Form.Control
													type = 'number'
													placeholder = 'ex. 1'
													value={quantity}
													onChange = {e => setQuantity(e.target.value)}
													required/>
												</Form.Group>
											</Col>
											<Col>
												<Form.Group className ='mb-1' controllId='quantity'>
													<Form.Label>Total: </Form.Label>
													<Form.Control
													type = 'number'
													placeholder = 'ex. 1'
													value={quantity*price}
													onChange = {e => setTotal(e.target.value)}
													required
													disabled/>
												</Form.Group>
											</Col>
										</Row>
												<Button variant="dark" type="submit"block>Add to Cart</Button>
												<Link className= 'btn btn-secondary m-3'to='/active-items' block> Back </Link>
										
									</Container>
									</>
									:
									<Link className= 'btn btn-secondary m-3'to='/active-items'> Back </Link>
								:
								<>
								<Link className ="btn btn-dark btn-block" to ="/login">Sign in</Link>
								<Link className= 'btn btn-secondary m-3'to='/active-items'> Back </Link>
								</>
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
		</Form>
		)
}